#include <iostream>
#include <cmath>

using namespace std;

int NOD(int n1, int n2)
{
    int div;
    if (n1 == n2)   // ���� ����� �����, ��� ������
        return n1;
    int d = n1 - n2; // ������� �������� �����
    if (d < 0)       // ���� �������� �������������,
    {
        d = -d;     // ������ ����
        div = NOD(n1, d); // �������� ������� NOD() ��� ���� ���������� �����
    }
    else      // ���� �������� n1-n2 �������������
    {
        div = NOD(n2, d); // �������� ������� NOD() ��� ���� ���������� �����
    }
    return div;
}


int main()
{
	setlocale(LC_ALL, "ru");
    int x, y;
    cout << "������ �����: ";
    cin >> x;
    cout << "������ �����: ";
    cin >> y;
    cout << "��� = " << NOD(x, y) << endl;
    cout << "�������� �� ����� ��������� ��������?" << endl;
    if (NOD(x, y) == 1)
    {
        cout << "��" << endl;
    }
    else
    {
        cout << "���" << endl;
    }

    system("Pause");
	return 0;
}